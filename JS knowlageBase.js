// Strict Mode
{
    'use strict';
}

// Scopes
{
    // Functional Scope
    {
        function func() {
            if (true) {
                var a = true;
            }
            console.log(a);
        }
        func();
        console.log(a);
    }

    // Global Scope
    {
        function func() {
            if (true) {
                a = true;
            }
            console.log(a);
        }
        func();
        console.log(a);
    }

    // Local Scope
    {
        function func() {
            if (true) {
                let a = true;
            }
            console.log(a);
        }
        func();
        console.log(a);
    }
}

// Number
{
    // Infinity
    {
        console.log(1 / 0);
        console.log(Number.MAX_VALUE + Number.MAX_VALUE);
        console.log(-Number.MAX_VALUE - Number.MAX_VALUE)
    }

    // toString
    {
        var a = 45;
        console.log(a.toString());
        console.log(a.toString(2));
        console.log((255).toString(16));
    }

    // Parsing
    {
        console.log(parseInt('100px'));
        console.log(parseFloat('21.5$'));
    }

    // Math
    {
        console.log(Math.floor(15.8));
        console.log(Math.ceil(15.8));
        console.log(Math.round(15.8));
        console.log(Math.trunc(15.8));
        console.log(Math.random());

        console.log(Math.max(7, -10, 2));
        console.log(Math.min(7, -10, 2));
        console.log(Math.pow(7, 2));
    }

    // toFixed
    {
        var a = 53.76;
        console.log(a.toFixed());
        console.log(a.toFixed(1));
        console.log(a.toFixed(4));
    }
}

// Template literals
{
    // Multiline strings
    {
        console.log('string text line 1\n' +
            'string text line 2');
        console.log(`string text line 1\n
string text line 2
            string text line 3`);
    }

    // Expression interpolation
    {
        var a = 5;
        var b = 10;
        console.log('Fifteen is ' + (a + b));
        console.log(`Fifteen is ${a + b}`);
    }

    // String.raw
    {
        let str1 = String.raw `C:\new`;
        var str2 = "C:\new";
        console.log(str1);
        console.log(str2);
    }
}

// Referece types
{
    // Primitive types
    {
        var a = 50;
        var b = a;
        var b = 100;

        console.log(a);
        console.log(b);
    }
    // Reference type
    // Shallow copy
    {
        var a = {
            foo: "bar"
        };
        var b = a;
        b.foo = "baz";
        console.log(a.foo);
        console.log(b.foo);
    }

    // Copy object not the reference
    // Deep copy
    {
        var a = {
            foo: "bar"
        };
        var b = Object.create(a);
        b.foo = "baz";
        console.log(a.foo);
        console.log(b.foo);
    }

}

// Common browser methods
{
    // Alert
    {
        alert('Example');
    }

    // Promp
    {
        var a = prompt('Some text');
        console.log(a);

        //NOTE
        // Cancel button will return null;

        a = prompt('Some text', 'Default value');
        console.log(a);
    }

    // Comfirm
    {
        var a = confirm('true or false?');
        console.log(a);
    }

}

// String
{
    //Upper lower case
    {
        var str = "Test";
        console.log(str.toLowerCase());
        console.log(str.toUpperCase());
    }

    // Char at
    {
        console.log(str[0]);
        console.log(str.charAt(1));
    }

    // Concat
    {
        var str1 = "test ";
        var str2 = "123";

        str3 = str1.concat(str2);
        console.log(str3);
        str4 = str1 + str2;
        console.log(str4);
    }

    // substr(startIndex, length)
    // substring(startIndex, endIndex)
    {
        var str = "Random 9";
        console.log(str.substr(3));
        console.log(str.substr(3, 2));

        console.log(str.substring(3, 5));
    }

    // indexOf and lastIndexOf
    {
        var str = "Random 9, random 9";
        console.log(str.indexOf('dom'));
        console.log(str.indexOf('dom', 7));
        console.log(str.indexOf('foo'));

        console.log(str.lastIndexOf('dom'));
        console.log(str.lastIndexOf('dom', 7));
        console.log(str.lastIndexOf('foo'));
    }

    // alphabetical character position
    {
        console.log('X'.localeCompare('Y'));
        console.log('Y'.localeCompare('Y'));
        console.log('Z'.localeCompare('Y'));
    }

    // trim
    {
        console.log(('    test    ').trim());
    }

    // reverse
    {
        var a = "String To Reverse";
        a.split("").reverse().join();
        console.log(a);
    }

    // Regex methods
    {
        // match
        {
            var str = 'Random 9 STRING testing';
            var regexp = str.match(/[T9]/ig);
            console.log(regexp);

            regexp = str.match(/[z]/ig);
            console.log(regexp);
        }

        // search
        {
            var str = 'Random 9 STRING testing';
            var position = str.search(/t/);
            console.log(position);
        }

        // replace
        {
            var url = "test.com/random=9";
            var newUrl = url.replace(/random/g, 'foo');
            console.log(newUrl);
            console.log(url);
        }
    }

}

// Loops
{
    zLoop: for (var z = 0; z < 10; z++) {
        iLoop: for (var i = 0; i < 10; i++) {
            for (var y = 0; y < 10; y++) {
                if (z === 6 && i === 6 && y === 6) {
                    break zLoop;
                }
                if (y % 2 === 1) {
                    continue;
                }
                if (i % 2 === 1) {
                    continue iLoop;
                }
                if (z % 2 === 1) {
                    continue zLoop;
                }
                console.log(z, i, y);
            }
        }
    }
}

// Array
{
    //Is array
    {
        var b = new Array();
        var a = [];
        var c = ["test", "array"];

        console.log(Array.isArray(a));
        console.log(Array.isArray(b));
        console.log(Array.isArray(c));
    }

    // join array
    {
        var c = ["test", "array"];
        c.join();
        c.join(' - ');
    }
    // Reverse array
    {
        function reverse(str) {
            var stack = [];
            for (var i = 0; i < str.length; i++) {
                stack.push(str[i]);
            }

            var reverseStr = '';
            while (stack.length > 0) {
                reverseStr = reverseStr + stack.pop();
            }
            console.log(reverseStr);

            //or
            reverseStr = stack.join('');
            console.log(reverseStr);
        }

        reverse('JavaScript');
    }

    // Push and pop
    {
        // Stack
        var stack = [];

        stack.push(1);
        stack.push(2);
        stack.push(3);
        console.log(stack);
        console.log(stack.pop());
        console.log(stack.pop());
        console.log(stack);
        console.log(stack.pop());
        console.log(stack.pop()); // undefined
        console.log(stack);
    }

    // Unshift and Shift
    {
        // Queue
        var num = '5678';
        var queue = [];

        // Insertion
        for (var i = 0; i < num.length; i++) {
            console.log(num[i]);
            queue.unshift(num[i]);
        }
        console.log(queue);

        // Removal
        while (queue.length > 1) {
            queue.shift();
        }
        console.log(queue);

    }
    // Splice
    {
        var a = [1, 2, 3, 4];
        var removed = a.splice(2, 1);
        console.log(a);
        console.log(removed);

        var a = [1, 2, 3, 4];
        var replaced = a.splice(2, 1, 7, 8, 9); //from, how many, new elements
        console.log(a);
        console.log(replaced);
    }
    // Slice
    {
        var a = [1, 2, 3, 4];
        var b = a.slice();
        var c = a.slice(2);
        var d = a.slice(2, 1);
        console.log(b);
        console.log(c);
        console.log(d);
    }

    // Reference type
    // Shallow copy
    {
        var a = ["foo", "bar"];
        var b = a;
        b[0] = "baz";
        console.log(a);
        console.log(b);
    }

    // Copy object not the reference
    // Deep copy
    {
        var a = ["foo", "bar"];
        var b = Array.from(a);
        var c = a.slice();
        b[0] = "baz";
        c[0] = "fooz";
        console.log(a);
        console.log(b);
        console.log(c);
    }

    // mix values in array
    {
        var a = [{
            obj: 'some value'
        }, false, 'test', function () {
            console.log("Func")
        }];
        console.log(a[0].obj);
        console.log(a[3]());
    }

    // concat
    {
        var a = [1, 2];
        var b = [3, 4];
        var c = {
            length: 2,
            0: "test",
            1: "word"
        };
        var d = {
            [Symbol.isConcatSpreadable]: true,
            length: 4,
            0: "test",
            1: "word",
            2: {
                length: 2,
                0: "test",
                1: "word"
            },
            3: {
                [Symbol.isConcatSpreadable]: true,
                length: 2,
                0: "test",
                1: "word"
            }
        };
        console.log(a.concat(b));
        console.log(a.concat(c));
        console.log(a.concat(d));
    }
    // reverse
    {
        var array1 = [3, 4, 5, 6, 7];
        var reversed = array1.reverse();

        console.log(array1); // [7, 6, 5, 4, 3]
        console.log(reversed); // [7, 6, 5, 4, 3]
    }
    // sort
    {
        var a = [1, 5, 3, 7, 2, 9, 4];
        a.sort();
        console.log(a);
        a.reverse();
        console.log(a);
        a = ["t", "J", "T", "n", "z", "a"];
        a.sort(function (a, b) {
            return a.toLowerCase() > b.toLowerCase() ? 1 : -1;
        });
        console.log(a);
    }
    // map
    {
        var a = [1, 5, 3, 7, 2, 9, 4];
        var b = a.map(function (a) {
            return a * a;
        })
        console.log(b);
    }
}

// Functions
{
    // Function expresions are not hoisted!
    {
        function d(str) {
            console.log(str)
        }
        d('d');

        c('c');

        function c(str) {
            console.log(str)
        }

        var b = function (str) {
            console.log(str)
        }
        b('b');

        a('a');
        var a = function (str) {
            console.log(str)
        }
    }

    // IIFE
    {
        (function () {
            console.log("Hi");
        })();
    }

    // new Function
    {
        var a = new Function('a', 'b', 'return a+b');
        a(2, 7);
    }

    // this context
    {
        //this
        {
            console.log(this === window);
            var x = "val"
            console.log(window.x);
            console.log(x);
        }

        //set content of this by call and aply
        {
            var obj = {
                userName: "Jon",
                ureAge: 21
            }

            function welcome(greetings, title) {
                console.log(greetings);
                console.log(`${title} ${this.userName}, we know you are ${this.ureAge}`);
            }

            welcome("Welcome!", "Sir");
            welcome.apply(obj, ["Welcome!", "Sir"]); // pass content of this and array of arguments
            welcome.call(obj, "Welcome!", "Sir"); // pass content of this and arguments
            welcome.bind(obj)("Welcome!", "Sir"); // set content of this call function with arguments
        }
    }
    //arguments
    {
        console.log(sum(14, 7, 83, 12, 44, 54));

        function sum() {
            return Array.from(arguments).reduce(function (a, b) {
                return a + b
            });
        }
    }

}



// NEED TO KNOW MORE ABOUT
{
    Symbol();
}