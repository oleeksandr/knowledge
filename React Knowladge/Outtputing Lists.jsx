// **************************************************
// ==================== Outputting Lists: ====================
// **************************************************
state = {
    val: [
        { name: "test", age: 30, id: 1 },
        { name: "test2", age: 31, id: 2 }
    ]
}

return (
    <div>
        <h1>Other content</h1>
        {this.state.val.map((record, index) => {
            return <SomeComponent
                click={this.someFunction(index)}
                key={record.id}
                name={record.name}
                age={record.age} />
        })}
    </div>
);