// **************************************************
// ==================== State usage in components ====================
// **************************************************
// NOTE: State is to only keep own not passed properties
// ========== Class-base ==========
state = {
    val1 = 'test',
    val2: [
        { name: "test", age: 30 },
        { name: "test2", age: 31 }
    ]
}

this.state.val2[1].name;

this.setState({
    val2: [
        { name: "zztest", age: 20 },
        { name: "zztest2", age: 21 }
    ]
})

// If you update your state depending on current state keep in mind that it's asynchronously action
// to do that use following syntax
this.setState((prevState, props) => {
    return {
        persons: updatedPersons,
        count: prevState.count + 1
    }
});

// ========== Function-base ==========

import React, { useState } from 'react';
const [val1, setVal1] = useState({
    val1 = 'test'
})

const [val2, setVal2] = useState({
    val2: [
        { name: "test", age: 30 },
        { name: "test2", age: 31 }
    ]
})

val2[1].name;

setVal2({
    val2: [
        { name: "zztest", age: 20 },
        { name: "zztest2", age: 21 }
    ]
})