// **************************************************
// ==================== Async Component Loading ====================
// **************************************************

// If you want to load component async not as a part of a bundle but as a separate chunk to light down your app at start if component that you load is not required at the beginning

// Create a Async component like this
import React, { Component } from 'react';

const AsyncComponent = (importComponent) => {
    return class extends Component {
        state = {
            component: null
        }

        componentDidMount() {
            importComponent()
                .then(cmp => {
                    this.setState({ component: cmp.default });
                });
        }

        render() {
            const C = this.state.component;
            return C ? <C {...this.props} /> : null;
        }
    }
}

export default AsyncComponent;

// and then use it in the app like following

import React, { Component } from 'react';
import AsyncComponent from '../../hoc/asyncComponent';
const AsyncNewPost = AsyncComponent(() => {
    return import('./NewPost/NewPost');
});

class Test extends Component {
    state = {
        loaded: false
    }

    loadDynamic = () => {
        this.setState({ loaded: true });
    }

    render() {
        return (
            <div className="Test">
                <button onClick={this.loadDynamic}>Load Component Dynamically </button>
                {
                    this.state.loaded
                        ? <AsyncNewPost />
                        : <h1>Hidden</h1>
                }
            </div>

        );
    }
}

export default Test;

// After React 16.6 you can use build in lazy loading method React.lazy();
import React, { Component, Suspense } from 'react';
const Posts = React.lazy(() => import('./containers/Posts'));
// and use it like
<Suspense fallback={<div>Loading...</div>}>
    <Posts />
</Suspense>

// So you no longer need to create an async component you can use React.lazy instead. The full code can be like:
import React, { Component, Suspense } from 'react';

import User from './containers/User';

const Posts = React.lazy(() => import('./containers/Posts'));

class App extends Component {
    state = { showPosts: false };

    modeHandler = () => {
        this.setState(prevState => {
            return { showPosts: !prevState.showPosts };
        });
    };

    render() {
        return (
            <React.Fragment>
                <button onClick={this.modeHandler}>Toggle Mode</button>
                {this.state.showPosts ? (
                    <Suspense fallback={<div>Loading...</div>}>
                        <Posts />
                    </Suspense>
                ) : (
                        <User />
                    )}
            </React.Fragment>
        );
    }
}

export default App;