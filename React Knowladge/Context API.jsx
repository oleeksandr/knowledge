// **************************************************
// ==================== Context API ====================
// **************************************************
// Context Api give you a way to pass properties in nested components without passing it throw all his parents
// For example you have a structure like the following:
// app -> lvl1 -> lvl2 -> lvl3 -> destination
//     |->start
//     |->other
// and you want to pass changed props from 'start' to 'destination'
// without Context API you will need to pas params to the app then to lvl1 than to lvl2 and so on


// With Context Api you can do it in following way
import React from 'react';
const GlobalContext = React.createContext({
    authenticated: false,
    login: () => { }
});
export default GlobalContext;

// In app component you set up a provider to this global content
import GlobalContext from "GlobalContext";
return (
    <p>other staff not that not need access to global content</p>
    <OTHER />
    <GlobalContext.Provider value={{
        login: this.loginHandler,
        authenticated: this.state.authenticated
    }}>
        <LVL1 />
        <START />
    </GlobalContext.Provider>
);
// In the Provider value you can set an actual value instead of default one
// and than in LVL1 and START you can access them like this:

import GlobalContext from "GlobalContext";
constructor(props){
    super(props);
}
<GlobalContext.Consumer>
{context => context.authenticated? <p>Authenticated!</p>: <p>Need to Log In</p>}
</GlobalContext.Consumer>
// Note that consumer not return a JSX but a function instead

// Better way of accessing property is to use a static contextType:
import GlobalContext from "GlobalContext";
constructor(props){
    super(props);
}
static contextType = GlobalContext;
{this.context.authenticated? <p>Authenticated!</p>: <p>Need to Log In</p>}
// using contextType allow you to access context even at life cycle functions like contentDidMount
// NOTE: static contextType and this.context will be required naming provided from React

// In the functional components we can use useContext hook:
import React, {useContext} from 'react';
const funcComp = props => {
    const globalContext = useContext(GlobalContext);
    return (
        <button onClick={globalContext.login}>Log in</button>
    )
}