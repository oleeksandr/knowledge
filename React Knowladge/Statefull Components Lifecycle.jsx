// **************************************************
// ==================== Statefull Components Lifecycle: ====================
// **************************************************

// *************************
// ========== Creation ==========
// *************************
// Flow:
// constructor(props) => getDerivedStateFromProps(props, state) => render() => Render Child Components => componentDidMount()


// ========== constructor(props) ==========
// Set up State
constructor(props) {
    super(props); //important
    console.log("[APP JS] constructor");
    this.state = {
        param: false,
        param2: "false"
    }
}

// ========== getDerivedStateFromProps(props, state) ==========
// Sync state
static getDerivedStateFromProps(props, state) {
    console.log('[APP JS] getDerivedStateFromProps', props);
    return state;
}
// ========== render() ==========
// Prepare & structure your JSX code
render() {
    let content = null;

    if (this.state.someValue) {
        content = (
            <h1>Hi</h1>
        );
    }
    return (
        <div>
            <h1>Other content</h1>
            {content}
        </div>
    );
}

// ========== Render Child Components ==========


// ========== componentDidMount() ==========
// Send HTTP requests
componentDidMount() {
    console.log('[APP JS] componentDidMount');
}


// *************************
// ========== Update ==========
// *************************
// Flow:
// getDerivedStateFromProps(props, state) => shouldComponentUpdate(props, state) => render() => Update Child Components props => getSnapshotBeforeUpdate(prevProps, prevState) => componentDidUpdate()


// ========== getDerivedStateFromProps(props, state) ==========
// Sync state

// ========== shouldComponentUpdate(props, state) ==========
// Decide where update your component and where not
// If you checking each of the props to be changed in shouldComponentUpdate you can use PureComponent:
// When you declaring the class base component you will need to extend PureComponent instead of Component and result will be the same but in less code
shouldComponentUpdate(nextProps, nextState) {
    console.log('[RoundButton JS] shouldComponentUpdate..');
    return (this.props.specialClass !== nextProps.specialClass || this.props.round !== nextProps.round);
}

// ========== render() ==========
// Prepare & structure your JSX code

// ========== Update Child Components props ==========

// ========== getSnapshotBeforeUpdate(prevProps, prevState) ==========
// last-minute DOM operations
getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log('[RoundButton JS] getSnapshotBeforeUpdate');
    return { message: "Message getted from getSnapshotBeforeUpdate" };
}

// ========== componentDidUpdate() ==========
// After your component update
componentDidUpdate(prevProps, prevProps, snapshot) {
    console.log('[RoundButton JS] componentDidUpdate');
    console.log(snapshot);
}

// *************************
// ========== Unmount ==========
// *************************
// ========== componentWillUnmount() ==========
// Clean up work before component removing like removing event listeners
componentWillUnmount() {
    console.log('[RoundButton JS] componentWillUnmount');
}