// **************************************************
// ==================== Styling: ====================
// **************************************************
// Inline styling can be set up for individual elements
// NOTE: inline style don't have pseudo selectors and media queries (as normal inline styles)
const style = { color: "red" }
if (this.state.someState) {
    style.color = "blue";
}
return (
    <div>
        <div style={{ background: "red" }}></div>
        <div style={style}></div>
    </div>
);

// Classnames
const classLists = ["btn"];
let classLists2 = "btn";
if (this.state.someState) {
    classLists.push("primary");
    classLists2 += " primary";
}
return (
    <div>
        <div className="btn primary"></div>
        <div className={classLists2}></div>
        <div className={classLists.join(' ')}></div>
    </div>
);

// Impost classes from style (CSS Modules)
// There is an option to import and use classnames instead of .css file
// The syntax will be following:
// 
// import classes from './Post.css';
// const post = () => (
//     <div className={classes.Post}>...</div>
// );
// 
// To achieve this you need additional setUp in config files
// Check how to in: Enabling & Using CSS Modules

// test: cssRegex,
// exclude: cssModuleRegex,
// use: getStyleLoaders({
//   importLoaders: 1,
//   sourceMap: isEnvProduction && shouldUseSourceMap,
//   modules: true,
//   localIdentName: "[name]__[local]___[hash:base64:5]"
// }),

// and in app use following:
import styles from './App.css';
<div className={styles.App}></div>
