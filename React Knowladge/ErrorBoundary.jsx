// **************************************************
// ==================== ErrorBoundary ====================
// **************************************************
// Used to show some error message if some fetch from external source return error and to not break the app execution 
import React, { Component } from 'react';
class ErrorBoundary extends Component {
    state = {
        hasError: false,
        errorMessage: ""
    }
    componentDidCatch = (error, info) => {
        this.setState({
            hasError: true,
            errorMessage: error
        })
    }
    render() {
        if (this.state.hasError) {
            return <h1>ERROR</h1>
        } else {
            return this.props.children
        }
    }
}
export default ErrorBoundary;

// Usage Example
//<ErrorBoundary>
//    <MyWidget />
//</ErrorBoundary>