// **************************************************
// ==================== Animation ====================
// **************************************************
// npm i -s react-transition-group
// https://reactcommunity.org/react-transition-group/transition

// In case you want to add and animation to you components but still want to render them conditionally we can use additional package react-transition-group
// It allow us to add the animation after component added to dom and before it will be removed based on transition

// **************************************************
// ========== Usage ==========
// **************************************************
import Transition from 'react-transition-group';

<Transition in={this.state.showBlock} timeout={{
    appear: 0,
    enter: 1000,
    exit: 1000
}} mountOnEntering unmountOnExit>
    {state => (
        <div style={{
            background: "red",
            width: "100px",
            height: "100px",
            margin: "0 auto",
            transition: "opacity 1s ease-out",
            opacity: (state === "entered") ? "1" : "0"
        }} />
    )}
</Transition>

    // We just wrap our conditional element and set the conditional property on in property (expect boolean)
    // We can define that it will be added to dome and removed from it by passing two additional props mountOnEntering unmountOnExit
    // In the timeout we can pass a time in milliseconds or and object for specific animation
    // Note that we don't wrap component itself in the Transition but we expecting a function that will receive a state of the animation
    // Based on that state we can manage our animation by checking what is it equal to
    // We can also pass a function that will be called when the state will be changed:
    // onEnter, onEntering, onEntered onExit, onExiting, onExited

    // In case we just want a css classes to be added to manage the transition we can use CSSTransition
    <CSSTransition in={inProp} timeout={200} classNames="my-node">
        <div>
            I'll receive my-node-* classes
    </div>
    </CSSTransition>
    // where the wrapped element receive the class defined in classNames with state (my-node-enter, my-node-exit...)

    // To animate a list we can use a TransitionGroup
    // The <TransitionGroup> component manages a set of transition components (<Transition> and <CSSTransition>) in a list.

    <Container style={{ marginTop: '2rem' }}>
        <ListGroup style={{ marginBottom: '1rem' }}>
            <TransitionGroup className="todo-list">
                {items.map(({ id, text }) => (
                    <CSSTransition
                        key={id}
                        timeout={500}
                        classNames="item"
                    >
                        <ListGroup.Item>
                            <Button
                                className="remove-btn"
                                variant="danger"
                                size="sm"
                                onClick={() =>
                                    setItems(items =>
                                        items.filter(item => item.id !== id)
                                    )
                                }
                            >
                                &times;
                </Button>
                            {text}
                        </ListGroup.Item>
                    </CSSTransition>
                ))}
            </TransitionGroup>
        </ListGroup>
        <Button
            onClick={() => {
                const text = prompt('Enter some text');
                if (text) {
                    setItems(items => [
                        ...items,
                        { id: uuid(), text },
                    ]);
                }
            }}
        >
            Add Item
      </Button>
    </Container> 