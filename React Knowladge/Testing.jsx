// **************************************************
// ==================== Testing ====================
// **************************************************


// **************************************************
// ========== Installation ==========
// **************************************************

// npm i -s enzyme react-test-renderer enzyme-adapter-react-16


// **************************************************
// ========== Tests ==========
// **************************************************

// Our test we create in the same directory as a tested part and naming file like ~componentName~.test.js

// To test JS we can use the Jest tool that is already in package.json
// https://jestjs.io/docs
// Here we describe our test cases in "describe" function to tell what component or part we are testing
// next we describe what our test is suppose to do with "it" function
// as a test function we expect some results so call "expect" and then specify what we are expecting


// To test some components without rendering the whole app or whole component we can use enzyme
// https://enzymejs.github.io/enzyme/docs/api/
// "shallow" help us to render component without it nested child components and to testing a component as a unit


// Run tests with "npm test" command

// **************************************************
// ========== Test Examples ==========
// **************************************************

// Example #1
// Test on component conditional rendering
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import NavigationItems from './NavigationItems';
import NavigationItem from './NavigationItem/NavigationItem';

configure({ adapter: new Adapter() });

describe('<NavigationItems />', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(<NavigationItems />);
    })

    it('should render three <NavigationItem /> elements if not authenticated', () => {
        expect(wrapper.find(NavigationItem)).toHaveLength(3);
    });

    it('should render three <NavigationItem /> elements if authenticated', () => {
        wrapper.setProps({ isAuth: true })
        expect(wrapper.find(NavigationItem)).toHaveLength(3);
    });

    it('should an exact logout button', () => {
        wrapper.setProps({ isAuth: true })
        expect(wrapper.contains(<NavigationItem link="/logout"> Sign Out </NavigationItem>)).toEqual(true);
    });
});


// Example #2
// Test on component conditional rendering
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { BurgerBuilder } from './BurgerBuilder';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';

configure({ adapter: new Adapter() });

describe('<BurgerBuilder />', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(<BurgerBuilder onInitIngredients={() => {}}/>);
    })

    it('should render <BuildControls /> when receiving ingredients', () => {
        wrapper.setProps({
            ings: {
                salad: 1,
                cheese: 2,
                bacon: 2,
                meat: 1
            }
        })
        expect(wrapper.find(BuildControls)).toHaveLength(1);
    });
});


// Example #3
// Redux test
import reducer from './auth';
import * as actionTypes from '../actions/actionTypes';

describe('auth reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            token: null,
            userId: null,
            error: null,
            loading: false,
            authRedirectPath: '/'
        });
    });

    it('should store the token upon login', () => {
        expect(reducer({
            token: null,
            userId: null,
            error: null,
            loading: false,
            authRedirectPath: '/'
        }, {
            type: actionTypes.AUTH_SUCCESS,
            token: "Random 9",
            userId: "123"
        })).toEqual({
            token: "Random 9",
            userId: "123",
            error: null,
            loading: false,
            authRedirectPath: '/'
        });
    });
});