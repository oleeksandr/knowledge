// **************************************************
// ==================== React Hooks ====================
// **************************************************

// React hooks is a functions that can be used only inside a function base classes
// They allow to have a state and lifecycle events


// **************************************************
// ========== useState ==========
// **************************************************
// useState is used when your component need to have their own state
// it's similar to state in class based component but have a difference:
// 1. The only argument to the useState() Hook is the initial state. Unlike with classes, the state doesn’t have to be an object
// 2. It returns a pair of values: the current state and a function that updates it.
// 3. Setter replace the original object, so make sure you don't loose some properties while update
// 4. You can use multiple useState to store values

// Example:
// Before
const [getState, setState] = useState({ title: "", amount: "" });
return (
    <div>
        <div className="form-control">
            <label htmlFor="title">Name</label>
            <input type="text" id="title" value={getState.title} onChange={event => { const newVal = event.target.value; setState((prevState) => ({ ...prevState, title: newVal, })) }} />
        </div>
        <div className="form-control">
            <label htmlFor="amount">Amount</label>
            <input type="number" id="amount" value={getState.amount} onChange={event => { const newVal = event.target.value; setState((prevState) => ({ ...prevState, amount: newVal, })) }} />
        </div>
    </div>
);

// After
const [getTitle, setTitle] = useState("");
const [getAmount, setAmount] = useState("");
return (
    <div>
        <div className="form-control">
            <label htmlFor="title">Name</label>
            <input type="text" id="title" value={getTitle} onChange={event => setTitle(event.target.value)} />
        </div>
        <div className="form-control">
            <label htmlFor="amount">Amount</label>
            <input type="number" id="amount" value={getAmount} onChange={event => setAmount(event.target.value)} />
        </div>
    </div>
);


// **************************************************
// ========== useCallback ==========
// **************************************************
// useCallback is used to avoid function initialization on rerender fo if if passed as a props it still be the same props

const setFetchedIngredients = useCallback(responseIngredients => {
    setIngredients(Object.keys(responseIngredients).map((ingredientName) => ({ ...responseIngredients[ingredientName], id: ingredientName })));
}, []);


// **************************************************
// ========== useReducer ==========
// **************************************************
// useReducer is a more elegant way to manage state 
// It is useful when we need to manage multiple states by the same purpose
// Or to keep complex logic of state updates in one place
// Or if your state depends on the previous state

const ingredientReducer = (currentIngredients, action) => {
    switch (action.type) {
        case "SET":
            return action.ingredients
        case "ADD":
            return [...currentIngredients, action.ingredient]
        case "REMOVE":
            return currentIngredients.filter(ingredient => ingredient.id !== action.id)
        default:
            throw new Error("Should not get there");
    }
}

// const [getIngredients, setIngredients] = useState([]);
const [getIngredients, dispatchIngredients] = useReducer(ingredientReducer, []);

// Call
dispatchIngredients({ type: "ADD", ingredient: { id: responseBody.name, ...ingredient } });


// **************************************************
// ========== createContext and useContext ==========
// **************************************************
// To pass our props direct to some components we can use Context like in case of class base components
// We creating a Context with createContext and setting up a default parameters
// Then we preparing context provider to trigger updates whenever data that is passed as a value prop will change
// When we need to get the data we can use useContext hook and get the data from our Context

// AuthContext
import React, { createContext, useState, useMemo } from 'react';
export const AuthContext = createContext({
    inAuth: false,
    login: () => { }
});
const AuthContextProvider = props => {
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const loginHandler = () => {
        setIsAuthenticated(true);
    }
    return (
        <AuthContext.Provider
            value={{ login: loginHandler, inAuth: isAuthenticated }}>
            {props.children}
        </AuthContext.Provider>
    );
};
export default AuthContextProvider;


// App 
const App = props => {
    const authContext = useContext(AuthContext);
    let content = <Auth />
    if (authContext.inAuth) {
        content = <Ingredients />
    }
    return content;
};
export default App;


// Auth
const Auth = props => {
    const authContext = useContext(AuthContext);
    const loginHandler = () => {
        authContext.login();
    };
    return (
        <div className="auth">
            <Card>
                <h2>You are not authenticated!</h2>
                <p>Please log in to continue.</p>
                <button onClick={loginHandler}>Log In</button>
            </Card>
        </div>
    );
};
export default Auth;

// index
ReactDOM.render(
    <AuthContextProvider>
        <App />
    </AuthContextProvider>,
    document.getElementById('root'));


// **************************************************
// ========== useMemo ==========
// **************************************************
// useMemo(() => computeExpensiveValue(a, b), [a, b]); hook is alternative to React.memo(<Comp />)


// **************************************************
// ========== useRef ==========
// **************************************************
// Creates a reference to the element on the page
function TextInputWithFocusButton() {
    const inputEl = useRef(null);
    const onButtonClick = () => {
      // `current` вказує на примонтований елемент поля вводу тексту
      inputEl.current.focus();
    };
    return (
      <>
        <input ref={inputEl} type="text" />
        <button onClick={onButtonClick}>Фокусуватись на полі вводу</button>
      </>
    );
  }