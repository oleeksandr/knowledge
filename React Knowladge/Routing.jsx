// **************************************************
// ==================== Routing ====================
// **************************************************

// **************************************************
// ========== Installing ==========
// **************************************************

// npm install --save react-router-dom
// Where in the router is logic and react-dom allow you to add it to the dom 


// **************************************************
// ========== Adding ==========
// **************************************************

// In the App.js or indexed.js routing should be added by wrapping it with BrowserRouter
import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';

import Blog from './containers/Blog/Blog';

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <div className="App">
                    <Blog />
                </div>
            </BrowserRouter>
        );
    }
}

export default App;

// If your app is working not from the root part Ex. example.com/my-app/ but not a example.com/ you can add basename to BrowserRouter


// **************************************************
// ========== Using ==========
// **************************************************
// You can add routing like following:
import { Route } from 'react-router-dom';
// ...
<Route path="/" exact render={() => <h1>Hello</h1>} />
// The Route will be replaced with the content that you define in the render function for it

// To render the component use component parameter
<Route path="/posts" exact component={Posts} />

// IMPORTANT: make sure that your server always return the index.js file even for unknown routing since your app now manage routing like /posts but your server know nothing about /posts


// **************************************************
// ========== Link ==========
// **************************************************

// To do not refresh page each time a link was clicked and don't lose the state we can use Link component like this:
import { Route, Link } from 'react-router-dom';

<li><Link to="/">Home</Link></li>
<li><Link to={{
    pathname: "/new-post",
    hash: "#section",
    search: "?query=true"
}}>New Post</Link></li>
// where you specify where you want to redirect

// Alternative way to redirect to different route using history.push
clickedHandler = (id) => {
    this.props.history.push({ pathname: "/" + id });
}
// This can be useful when you want to redirect user after some actions will finish (Ex. post requests);


// **************************************************
// ========== Style the active route ==========
// **************************************************

// If you want to style active link you can use NavLink instead of Link, it's adding class active automatically for us
<li><NavLink to="/" exact>Home</NavLink></li>
<li><NavLink to="/new-post">New Post</NavLink></li>

// At  NavLink "/" the word exact prevent that link to have class active even if we are at "/new-post" page
// The reason why it's working like that the same as for router it's check if it's starts with the "/" but with "exact" it's check if it exact that path

// If you want to change the active class just add activeClassName property to NavLink
<li><NavLink to="/new-post" activeClassName="my-active-class">New Post</NavLink></li>

// You can set up a styles for active NavLink from the JS side by adding property activeStyle to it

<li><NavLink to="/new-post" activeStyle={{
    color: 'orange'
}}>New Post</NavLink></li>


// **************************************************
// ========== withRouter ==========
// **************************************************

// Route also passing props to component like history, location and match
// If you want to use props list history and other at nested component you instead of passing them manually can use HOC "withRouter"
import { Route, Link, withRouter } from 'react-router-dom';
// ...
export default withRouter(Post);
// So now each your post will have props from the Route


// **************************************************
// ========== Path ==========
// **************************************************

// If at the routing you don't use "exact" it will check if your path start with path that you wrote
// for example
<Route path="/" render={() => <h1>Hello</h1>} />
// will render <h1>Hello</h1> for path "/test" since it's start with "/"

// You can use the same path multiple times

// Path of links are absolute
// to use relative path (add something to current path) you can use:
<Link to={props.match.url + '/new'}></Link>

// You can use dynamic path for examples for ids
<Route path="/:id" exact component={FullPost} />
// and at the FullPost you can access the if by:
componentDidMount() {
    console.log(this.props.match.params.id);
}

// Note that this should be the after defined rotes like "/new-post" since if it will be before the "/new-post" will not be reached it will be retrieved like "/:id"
// But even if it will be last route React will render you all path that match so to render only one route you can use Switch
<Route path="/" exact component={Posts} />
<Switch>
    <Route path="/new-post" exact component={NewPost} />
    <Route path="/:id" exact component={FullPost} />
</Switch>
// so now only one (first matched) route will be rendered

// You can add redirect to some path using Redirect
<Switch>
    <Route path="/new-post" exact component={NewPost} />
    <Route path="/:id" exact component={FullPost} />
    <Redirect from="/" to="/posts" />
</Switch>
// None that if you use Redirect outside of the Switch you can't set up a "from" parameter so it will always redirect if you don't use the condition like following:
let redirect = null;
if(this.StaticRange.redirect){
    redirect = <Redirect to="/posts" />
}
return(
    <div>
        {redirect}
        {/* ... */}
    </div>
);
// But this will do the same as
this.props.history.replace('/posts');
// NOTE: "replace" or redirect will replace current URL without saving the previous one to the history like in case of "push"

// **************************************************
// ========== Search and Fragment ==========
// **************************************************

<Link to="/my-path?start=5">Go to Start</Link>
// Getting the search parameters:
componentDidMount() {
    const query = new URLSearchParams(this.props.location.search);
    for (let param of query.entries()) {
        console.log(param); // yields ['start', '5']
    }
}

<Link to="/my-path#start-position">Go to Start</Link>
// Getting the fragment:
componentDidMount() {
    console.log(props.location.hash);
}


// **************************************************
// ========== Nested Routes ==========
// **************************************************

<Switch>
    <Route path="/new-post" exact component={NewPost} />
    <Route path="/posts" component={Posts} />
</Switch>

// Then in Posts you can set a nested route for a single Post
<Route path={props.match.url + "/:id"} component={Post} />


// **************************************************
// ========== Guard ==========
// **************************************************

// If user is not authenticated you may not alow them to access some path. Ex.:
{this.StaticRange.auth ? <Route path="posts" component={NewPost} /> : null}

// Or you can do it inside of the component
componentDidMount(){
    // if not auth => this.props.history.replace('/posts');
}


// **************************************************
// ========== 404 Page ==========
// **************************************************

// If you create a rote that have no path it will catch all path that possible
<Switch>
    <Route path="/posts" component={Posts} />
    <Route path="/new-post" exact component={NewPost} />
    <Redirect from="/" exact to="/posts" />
    <Route render={() => <h1>404</h1>} />
</Switch>
// so path "/test" will return 404