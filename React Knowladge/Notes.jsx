// **************************************************
// ==================== Important notes: ====================
// **************************************************
// 1. React component is custom HTML elements
// 2. Each (functional and class) components needs to return/ render some JSX code
// 3. JSX code expect only one root element to be returned (1 wrapper and all other inside of it)
// 4. In JSX tags that starts with lowercase like <div> or <h1> indicate that that is a native HTML elements and tags like <Div> or <App> is a your custom react components
// 5. props.children give you access to all what was passed between opening and closing tags <Example>Hi, I am a passed children</Example>
// 6. Hight order component it's component that wrap some other components
// 7. If you checking each of the props to be changed in shouldComponentUpdate you can use PureComponent:
//      When you declaring the class base component you will need to extend PureComponent instead of Component and result will be the same but in less code