// **************************************************
// ==================== Axios ====================
// **************************************************
// Axios is useful when you working with requests and returning the promise to you


// **************************************************
// ========== Requests ==========
// **************************************************
// GET
Axios.get("/posts")
    .then(response => {
        const posts = response.data.slice(0, 4);
        const updatedPosts = posts.map(post => {
            return {
                ...post,
                author: "Olek"
            }
        })
        this.setState({ posts: updatedPosts });
    });

// POST
Axios.post("/posts", post)
    .then(response => {
        console.log(response);
    });

// **************************************************
// ========== Setting global parameters ==========
// **************************************************
// Setting global parameters
Axios.defaults.baseURL = "https://jsonplaceholder.typicode.com";
Axios.defaults.h = "https://jsonplaceholder.typicode.com";
Axios.defaults.baseURL = "https://jsonplaceholder.typicode.com";


// **************************************************
// ========== Interceptors ==========
// **************************************************
// Interceptors is reached before to each of the requests/responses
Axios.interceptors.request.use(request => {
    console.log(request);
    return request; //REQUIRED to don't block sending
}, error => {
    console.log(error);
    return Promise.reject(error);
})

Axios.interceptors.response.use(response => {
    console.log(response);
    return response; //REQUIRED
}, error => {
    console.log(error);
    return Promise.reject(error);
})

// To remove interceptors use eject
var myInterceptor = axios.interceptors.request.use(function () {/*...*/ });
axios.interceptors.request.eject(myInterceptor);

// **************************************************
// ========== Different default setups ==========
// **************************************************
// If you need to use for example two different baseurl or some other setup you can create them and use separately

import Axios from "axios";

const instance = Axios.create({
    baseURL = "some other baseURL"
})

instance.defaults.header.common['Authorization'] = "Token from the instance";

export default instance;