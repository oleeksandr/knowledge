// **************************************************
// ==================== Stateless Components Lifecycle: ====================
// **************************************************

// ========== useEffect() ==========
// Use effect is called on every render cycle
// May be used for http requests
// To run only on creation second parameter after function will be []
// To run only on some data change second parameter after function will be [someDataParameter]
// 
// To add cleanup work you can return function which will be invoked
// NOTE: this will run BEFORE the main useEffect function runs, but AFTER the (first) render cycle
// Can be used to cancel some events before
import React, { useEffect } from 'react';
const funcComponent = (props) => {
    useEffect(() => {
        console.log('[funcComponent] useEffect on every render cycle');
        return () => {
            console.log('[funcComponent] cleanup work in useEffect');
        }
    });

    useEffect(() => {
        console.log('[funcComponent] useEffect only on first time render');
        const timer = setTimeout(() => {
            alert('EX. of imitating retrieving some data');
        }, 1000);
        return () => {
            clearTimeout(timer);
            console.log('[funcComponent] cleanup work in useEffect on unmount');
        }
    }, []);

    useEffect(() => {
        console.log('[funcComponent] useEffect only on props.person or props.someData change');
        return () => {
            console.log('[funcComponent] cleanup work in useEffect related to changes in props data');
        }
    }, [props.person, props.someData]);

    export default funcComponent;
}


// ========== React.memo() ==========
// React.memo is a equivalent of the shouldComponentUpdate for class base components.
// if none of the props that you use is changed this component don't need a rerendering
import React, { useEffect } from 'react';
const funcComponent = (props) => {
// SOME CODE
}
export default React.memo(funcComponent);