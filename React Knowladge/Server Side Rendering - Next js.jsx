// **************************************************
// ==================== Server Side Rendering - Next.js ====================
// **************************************************


// Server site rendering is when the user visiting your page server returns prepared rendered html code.
// It is used to help the Search Engines know what is on your page instead of seeing spinner for example.
// Server side rendering render the page once and other rendering processes will be on a client side.



// **************************************************
// ========== Next.js ==========
// **************************************************
// https://nextjs.org/docs/getting-started
// npm i --s next

// Next.js is a react framework that helps to manage server side rendering.


// **************************************************
// ========== Pages and routing ==========
// **************************************************

// Next.js will serve each file in /pages under a pathname matching the filename.
// For example, /pages/about.js is served at site.com/about and /pages/index.js is served at site.com/.


// To make a a dynamic route we can use brackets ([]) in the page name like this: /pages/posts/[id].js
// it will be available in the query object inside of the router. Like this {router.query.id}


// **************************************************
// ========== Links and Router ==========
// **************************************************

// In Next.js we already have a links and can use them 
import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

const Test = () => {
    const router = useRouter();
    return (
        <div>
            {router.query.id}
            <h1>Test Page</h1>
            <p>Go To <Link href="/">Home page</Link></p>
            <button onClick={() => { router.push('/') }}>Got to Home page</button>
        </div>
    );
}

export default Test;


// **************************************************
// ========== Styling ==========
// **************************************************
< style jsx global > {`
      html,
      body {
        padding: 0;
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
          Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
      }

      * {
        box-sizing: border-box;
      }
    `}</style >


// **************************************************
// ========== 404 Error page ==========
// **************************************************

// By default we have a 404 page provided by Next but we can customize it by creating a "_error.js" file in /pages


// **************************************************
// ========== Initial Props ==========
// **************************************************
// In next js we can call a function to set our initial props BEFORE render a component
// It is useful to get some data from the server 

// class base example:
class IndexPage extends Component {
    static getInitialProps(context) {
        const promise = new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve({ appName: "Super App" });
            }, 1000);
        });
        return promise;
    }

    render() {
        return (
            <div>
                <h1>The Main Page of {this.props.appName}</h1>
            </div>
        )
    }
}

// function base example:
const authIndexPage = (props) => (
    <div>
        <h1>The Auth Index Page - {props.appName}</h1>
        <User name="Max" age={28} />
    </div>
);

authIndexPage.getInitialProps = context => {
    const promise = new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve({ appName: "Super App (Auth)" });
        }, 5000);
    });
    return promise;
};