// **************************************************
// ==================== Refs ====================
// **************************************************

// Refs can be used when you need reference to actual element in DOM

// For example you want to set up a focus on some input
// **************************************************
// ========== Class Base Component ==========
// **************************************************

constructor(props) {
    super(props);
    this.inputElementRef = React.createRef();
}
// to this input you will need to add attribute ref
<input ref={this.inputElementRef} />

componentDidMount() {
    this.inputElementRef.current.focus();
}


// if you use old version of React (< 16.3) than you that you will use the syntax
<input ref={(inputElem) => { inputElem.focus() }} type="text" />

// or to access from other place assign this to variable
<input ref={(inputElem) => this.inputElement} type="text" />
// and
componentDidMount() {
    this.inputElement.current.focus();
}


// **************************************************
// ========== Functional Base Component ==========
// **************************************************
import React, { useEffect, useRef } from 'react';
const functionalComponent = props => {
    const elementRef = useRef();

    useEffect(() => {
        elementRef.current.focus();
    }, []);

    return (
        <input ref={elementRef} type="text" />
    );
}