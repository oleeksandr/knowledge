// **************************************************
// ==================== Redux Advanced ====================
// **************************************************

// **************************************************
// ========== Middleware ==========
// **************************************************

// Middleware is a code that you hook in to a process which then executed as a part of the process without stopping it on execution and before your action will be done.
// Example of logger middleware:

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';

import counterReducer from './store/reducers/counter';
import resultReducer from './store/reducers/result';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const logger = store => {
    return next => {
        return action => {
            console.log('[Middleware] Dispatching', action);
            const result = next(action);
            console.log('[Middleware] next state', store.getState());
            return result;
        }
    }
}

const rootReducer = combineReducers({
    ctr: counterReducer,
    res: resultReducer
});

const store = createStore(rootReducer, applyMiddleware(logger));

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();

// If you have more than one middleware you simply pass them one by one like this:
applyMiddleware(logger, secondMiddleware);
// or
applyMiddleware(...middleware);

// **************************************************
// ========== Redux DevTools ==========
// **************************************************

// To use react dew tools we need to install chrome plugging and add some extra lines to connect redux store with that extension.
// In case of using it with middleware this code will look like:

import { createStore, applyMiddleware, compose } from 'redux';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, /* preloadedState, */ composeEnhancers(
    applyMiddleware(...middleware)
));

//NOTE: to make it available only in development and hide reduxDevTools from other people while app is deployed we can use the process.env.NODE_ENV like following:
const composeEnhancers = process.env.NODE_ENV === "development" ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose;





// **************************************************
// ==================== Async code with redux ====================
// **************************************************

// In redux you cant use async code in reducer since it's need to return state and not waiting for requests or timeouts.
// You also can't return the promise here since the reducer don't expect for this.
// Actions must be plain objects. We need to use custom middleware for async actions
// To achieve that we can use action creators. Action creators is a function that return an action.


// **************************************************
// ========== Thunk ==========
// **************************************************

// If we use "redux-thunk" for example we can call async code and then dispatch an action.
// Example:

// Before:
export const storeResult = (result) => {
    return {
        type: STORE_RESULT,
        result: result
    }
}

// After
export const saveResult = (result) => {
    return {
        type: STORE_RESULT,
        result: result
    }
}

export const storeResult = (result) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(saveResult(result));
        }, 2000);
    }
}
// And in the index.js we need to import thunk
import thunk from 'react-thunk';
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(logger, thunk)));

// Addition redux func can pass additional prop getState. It's a function by calling which we can access our state.
export const storeResult = (result) => {
    return (dispatch, getState) => {
        setTimeout(() => {
            console.log(getState().ctr.counter);
            dispatch(saveResult(result));
        }, 2000);
    }
}
// but it's better to pass the parameters to your action creators instead of receiving the data from the state

// Notes:
// If there is a way to hold your business logic in one place - hold it in reducer since this is a place for it to keep your action creator simple.


// **************************************************
// ========== Saga ==========
// **************************************************
// npm install --save redux-saga
// https://redux-saga.js.org/


// redux-saga is a library that helps to make application side effects like asynchronous things
// easier to manage, more efficient to execute, easy to test, and better at handling failures.

// Contrary to redux thunk, we can don't end up in callback hell, we can test your asynchronous flows easily and your actions stay pure.


// EXAMPLE:
// BEFORE:
// Action
export const initIngredients = () => {
    return dispatch => {
        axios.get("ingredients.json")
            .then(response => {
                dispatch(setIngredients(response.data));
            }).catch(error => {
                dispatch(fetchIngredientsFail());
            });
    }
}


// AFTER:
// Action
export const initIngredients = () => {
    return {
        type: actionTypes.INIT_INGREDIENTS
    }
}

// Saga
import { put } from 'redux-saga/effects';
import * as actions from '../actions/index';
import axios from '../../axios-orders';
export function* initIngredientsSaga(action) {
    try {
        const response = yield axios.get("ingredients.json");
        yield put(actions.setIngredients(response.data));
    } catch (error) {
        yield put(actions.fetchIngredientsFail());
    }
}
export function* watchBurgerBuilderSaga() {
    yield takeEvery(actionTypes.INIT_INGREDIENTS, initIngredientsSaga);
}

// index.js
const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    rootReducer, composeEnhancers(
        applyMiddleware(sagaMiddleware)
    )
);
sagaMiddleware.run(watchBurgerBuilderSaga);


// Here we moved our initIngredients action to saga
// In saga we use put instead of dispatch to return action.
// Now actions are clear and have only a simple return action object and requests are in sagas
// we have a watcher that will fire our saga on some action
// yield takeEvery(actionTypes.INIT_INGREDIENTS, initIngredientsSaga);
// we add our watcher to middleware and all works the same way