// **************************************************
// ==================== Rendering conditional content: ====================
// **************************************************
// ========== As a ternary operator ==========
{
    this.state.someValue ?
        <div>
            <h1>Hi</h1>
        </div> : null
}
// ========== As updated on a render ==========
let content = null;

if (this.state.someValue) {
    content = (
        <div>
            <h1>Hi</h1>
        </div>
    );
}
return (
    <div>
        <h1>Other content</h1>
        {content}
    </div>
);