// **************************************************
// ==================== Example of Components creation ====================
// **************************************************
// Simple statefull/smart (class-based) component
import React, { Component } from 'react';
class Example extends Component {
    render() {
        return (
            <div className="Example">
                <h1>Hi</h1>
            </div>
        )
    }
}
export default Example

// Functional components
// Simple stateless/presentational (functional-based) component
import React from 'react';
const Example = (props) => {
    return (
        <div className="Example">
            <h1>Hi</h1>
        </div>
    )
}
export default Example;