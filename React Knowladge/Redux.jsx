// **************************************************
// ==================== Redux ====================
// **************************************************

// **************************************************
// ========== About flow ==========
// **************************************************

// Redux is state managing package. It's working like a global application state with reducer, dispatcher and subscription.
// Your component use actions. Actions reach reducer which update the application state based on the previous state
// After that state trigger a subscriptions and your component can subscribe to some of them.


// **************************************************
// ========== Installation ==========
// **************************************************

// npm install --save redux


// **************************************************
// ========== Reducer ==========
// **************************************************

// Reducer is a place where you define what should be done in case of some action
// Reducer is a function with two parameters, an old state and an action.
// Then based on action type (defined field in a dispatcher) you can know what action is it and run code for it
// At the first run state is empty so we add an initialState as a default value
// Note that you need to return new object without mutating the original state so spread (...) can be useful but remember that if you have a deep nesting it will still have a reference but not an actual value

const rootReducer = (state = initialState, action) => {
    if (action.type === 'INC_COUNTER') {
        return {
            ...state,
            counter: state.counter + 1
        };
    }
    if (action.type === 'ADD_COUNTER') {
        return {
            ...state,
            counter: action.value
        };
    }
    return state;
};


// **************************************************
// ========== Store ==========
// **************************************************

// Store is a place where we store a state.
// Since reducer is very close related to our state we need to pass it as a parameter into a createStore function

const createStore = redux.createStore;
const store = createStore(rootReducer);
console.log(store.getState());


// **************************************************
// ========== Subscription ==========
// **************************************************

//Subscription function accept a function that will be invoked after state change

store.subscribe(() => {
    console.log('[Subscription]', store.getState());
})


// **************************************************
// ========== Dispatching Actions ==========
// **************************************************

// Actions is a trigger for a reducer to update a state.
// In dispatch method you pass an object that will be received in a reducer as an action.
// Convention for a type is capitalized text

store.dispatch({ type: 'INC_COUNTER' });
store.dispatch({ type: 'ADD_COUNTER', value: 9 });


// **************************************************
// ========== Adding Redux to React ==========
// **************************************************

import { Provider } from 'react-redux';
const initialState = { counter: 0 };
const reducer = (state = initialState, action) => {
    //  ...
    return state;
}

const store = createStore(reducer);
ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

// Now after creating a store we use a Provider that allow our app to use that store

// TO receive some data from the store we use a 'connect' at a component that need access to our global store.

import { connect } from 'react-redux';

class Counter extends Component {
    //  ...
    render() {
        return (
            { this.props.ctr }
        )
    }
}

const mapStateToProps = state => {
    return {
        ctr: state.counter
    };
}

export default connect(mapStateToProps)(Counter);

// In the example above we use a connector to get counter from the global store as a ctr and pass it to the component as a property

// To make some action we need to add a dispatcher
// Like with getting state connector will help us to do that

const mapDispatchToProps = dispatch => {
    return {
        onIncrementCounter: () => dispatch({ type: 'INCREMENT' })
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);

// So now we can use a 'onIncrementCounter' which will trigger an 'INCREMENT' activity
// To use it we just again just call it from the props
<CounterControl label="Increment" clicked={this.props.onIncrementCounter} />

// **************************************************
// ========== Outsourcing action types ==========
// **************************************************

// In case of complex applications to avoid typo we can extract action types to there own file
// We will store constant values that can be used in reducer and dispatcher after importing them.

// Action
export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';
export const STORE_RESULT = 'STORE_RESULT';
export const DELETE_RESULT = 'DELETE_RESULT';

// Example of Reducer
import * as actionTypes from './actions';
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INCREMENT:
        ///...
    }
}

// Example of Dispatcher
onIncrementCounter: () => dispatch({ type: actionTypes.INCREMENT })


// **************************************************
// ========== Splitting Reducers ==========
// **************************************************

// Managing reducers of complex application will be easier if split the reducer into parts by feature
// Even in the case of many reducers in the end they will be combined into one main reducer

import { createStore, combineReducers } from 'redux';
import counterReducer from './store/reducers/counter';
import resultReducer from './store/reducers/result';

const finalReducer = combineReducers({
    ctr: counterReducer,
    res: resultReducer
});
const store = createStore(finalReducer);

// To avoid the naming duplicates combineReducers function accept an object, so now access to reducers will be little different

const mapStateToProps = state => {
    return {
        ctr: state.ctr.counter,
        storedResults: state.res.results
    };
}

// Note that after that splitting reducer we now don't have an access from the one reducer to another. 
// Even they will be combined into a one reducer but they own state will no longer have values from the other states

// Example:

// Before:
const initialState = {
    count: 0,
    results: []
};
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.STORE_RESULT:
            return {
                ...state,
                results: state.results.concat({ id: new Date(), value: state.counter })
            };
    }
}

// After:
const initialState = {
    results: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.STORE_RESULT:
            return {
                ...state,
                results: state.results.concat({ id: new Date(), value: action.counter })
            };
        default:
            return state;
    }
}

// So now we waiting for counter to be passed as an action parameter 

// IMPORTANT NOTE:
// Default case is required while you have combined reducers.
// If your action is not in the current reducer it will need to be passed to another one by redux.