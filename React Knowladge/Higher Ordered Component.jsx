// **************************************************
// ==================== Higher Order Components (HOC) ====================
// **************************************************

// HOC is just a wrapper that add some extra functionality to your components


// For example you may write a HOC that add div with class name on top of your components
import React from 'react';
const WithClass = props => {
    <div className={props.className}>
        {props.children}
    </div>
}
export default WithClass;
// and than by writing the following code you will wrap your elements with div with class name you pass
<WithClass className="some-className other-class">
    <div>Hi there</div>
    <div>And there</div>
</WithClass>

// Or you might write in in deferent way
import React from 'react';
const withClass = (ComponentToWrap, className) => {
    return props => (
        <div className={className}>
            <ComponentToWrap {...props} />
        </div>
    );
}
export default withClass;
// and that use it for example like this
export default withClass(SomeOtherComponent, "some-class");