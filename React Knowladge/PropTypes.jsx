// **************************************************
// ==================== PropTypes ====================
// **************************************************

// If you work in big team or writing a library and some people may use your code incorrectly
// you may use PropTypes to let them know that something is wrong

// you can install it wia npm
// npm install --save prop-types

// after your component definition you can set up types of your props
// which will show the warning in console at development time that types tat you pass are incorrect
personalbar.propTypes = {
    click: propTypes.func,
    name: propTypes.string,
    age: propTypes.number,
    changed: propTypes.func
}