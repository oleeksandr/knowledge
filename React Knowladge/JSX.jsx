// **************************************************
// ==================== JSX ====================
// **************************************************
// JSX is NOT HTML
// JSX is just syntactic sugar for JavaScript
// JSX code will be compiled into JS code
React.createElement('div', { className: 'Example' }, React.createElement('h1', null, 'Hi'));


// You should return only 1 root JSX element inside of the component
return (
    <div>Allowed</div>
    // <div>But second one isn't</div>
)

// You can wrap it into one root element
return (
    <div>
        <div>Allowed</div>
        <div>And that one also</div>
    </div>
)


// Or you can return an array of elements (since array is one object) but with unique key attribute
// by which React will identify each element
return this.props.persons.map((person, i) => {
    return <Person key={person.id}
        name={person.name} />
});


// Or you can wrap your code into array itself since it will be compiled into separate React.createElement
// and separate element with commas and also you will need to add key attribute
return ([
    <div key="1">Allowed</div>,
    <div key="2">And that one also</div>
]);


// Or you can use Higher Order Component (HOC) to wrap your data without unnecessary div or array syntax
// First you need to create wrapper like this:

const auxiliary = props => props.children;
export default auxiliary;

// and than you can wrap your code with that HOC
import Auxiliary from '../somePlace';

return (
    <Auxiliary>
        <div>Allowed</div>
        <div>And that one also</div>
    </Auxiliary>
);
// and in the result output in HTML you will se only this two div's without wrapper on top of them


// From React 16.2 there is a built in component that manage that so you can use it like this
return (
    <React.Fragment>
        <div>Allowed</div>
        <div>And that one also</div>
    </React.Fragment>
);