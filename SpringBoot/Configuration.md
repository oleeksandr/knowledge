# Configuration

## `@ComponentScan`

In the case if we have some of our packages in different package then the app and all the subdirectories, or in the case when we have a lot of classes and want to help Spring in identifying what need to be loaded to context we can add `@ComponentScan` annotation.

Before the application boot class add `@ComponentScan`.

Example:

```java
@SpringBootApplication
@ComponentScan(basePackages = {"com.oleeksandr.jokeapp", "com.oleeksandr.otherapp"})
public class JokeApplication {
```

## `@Configuration` `@Bean`

`@Configuration` annotation will be managed to be fired by Spring. In the case os IoC we can get creation of the object out of the services and put it under a `@Bean` annotation. This Bean will be used to inject `ChuckNorrisQuotes` when we have a constructor with the `ChuckNorrisQuotes` class inside. By this we getting out of the creation representation of the class and giving a Spring an option to manage that.

```java
@Configuration
public class ChuckConfig {

    @Bean
    public ChuckNorrisQuotes chuckNorrisQuotes() {
        return new ChuckNorrisQuotes();
    }

}
```

<hr>

In the case then we have a multiple implementations of the interface with different annotation (that configures that classes) we can move that that annotations to separated configuration class.

```java
@Configuration
public class PetConfig {

    @Bean
    @Profile("cat")
    PetService catPetService (){
        return new CatPetServiceImpl();
    }

    @Bean
    @Profile({"dog", "default"})
    PetService dogPetService (){
        return new DogPetServiceImpl();
    }
}
```

```
public class DogPetServiceImpl implements PetService {
    @Override
    public String getPetType() {
        return "Dogs are the best!";
    }
}
```
Where previously it was annotated as a services.

```
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile({"dog", "default"})
@Service
public class DogPetServiceImpl implements PetService {
    @Override
    public String getPetType() {
        return "Dogs are the best!";
    }
}
```

Now all of the configurations are stored in one file that will be easily to manage in case of huge applications.

## `@Scope`

By default the `@Bean` scope is singleton. It means that you only one instance of that bean will be created and shared to all the places where that bean is used.

If we want to change it (for example to create new instance for each time that bean is called) we can use a `@Scope` annotation.

## `@Value`

`@Value` annotation tels to spring to pick up the value form the properties file and connect it with the following attribute.

```java
@Value("${property.name}")
```

## `@PropertySource`

As an example we can create a separate properties file named `datasource.properties` with some data and read that from the `@Configuration` file. `@PropertySource` annotation will specify the file the properties that will be read.

```properties
datasource.user.username=Olek
datasource.user.password=Lis
datasource.connection.url=google.com
```

```java
@Configuration
@PropertySource("classpath:datasource.properties")
public class DatasourceConfig {

    @Value("${datasource.user.username}")
    String userName;

    @Value("${datasource.user.password}")
    String password;

    @Value("${datasource.connection.url}")
    String url;

    @Bean
    DatasourceModel prepareDatasourceModel() {
        DatasourceModel datasourceModel = new DatasourceModel();
        datasourceModel.setUsername(userName);
        datasourceModel.setPassword(password);
        datasourceModel.setUrl(url);
        return datasourceModel;
    }
}
```

To use multiple properties files we can specify the `@PropertySource` with multiple sources or we can use a `@PropertySources` with `@PropertySource` inside what is more clear to read.

```java
@PropertySource({"classpath:datasource.properties", "classpath:otherDatasource.properties"})
```
```java
@PropertySources({
        @PropertySource("classpath:datasource.properties"),
        @PropertySource("classpath:otherDatasource.properties")
})
```

Note: The environment or command line argument variables can override the properties file if they have the same name even if this is not specified. This may be the way of passing the parameters to the application from different environments (test, QA, prod).

[Documentation about this note](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config)

## `Environment`

We can access the OS environment variables by adding Environment variable. then we can use the `getProperty` method to get value of that parameter.

```java
@Autowired
Environment env;
```

and can be used as

```java
env.getProperty("env_parameter");
```

## Config application.properties based on active profile

If we set up an active profile inside of the `application.properties` (or yml) file we can also create a separate file with configuration for that profile that will override the default property with the same name in the  `application.properties`. 

application.properties:

```properties
spring.profiles.active=de

datasource.user.username=Ignored
```

application-de.properties:

```properties
datasource.user.username=Used
```
