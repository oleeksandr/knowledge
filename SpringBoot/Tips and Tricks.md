# Tips and Tricks

## Custom Banner

To replace spring logo during the application boot up we need to add a banner.txt into the resources folder. Content of that file will be loaded as a banner for our application.

Here is an example of this file:
```
       ___    _                 _                                _
      / _ \  | |   ___    ___  | | __  ___    __ _   _ __     __| |  _ __
     | | | | | |  / _ \  / _ \ | |/ / / __|  / _` | | '_ \   / _` | | '__|
     | |_| | | | |  __/ |  __/ |   <  \__ \ | (_| | | | | | | (_| | | |
      \___/  |_|  \___|  \___| |_|\_\ |___/  \__,_| |_| |_|  \__,_| |_|

```

Or in the case we want to convert the image to banner we can add an image file to resources folder and specify the name of it in the `application.properties` file after the `spring.banner.image.location=` property.