# Dependency Injection

Dependency injection is when the framework will be managing what need to be done on your demand

## Spring Context

Application Context will contain all beans that was created with annotations like:

- @Controller
- @Service
- @Component
- etc.

This will inject then when needed into your code.

Example:

```java
@Controller
public class MyController {
    public String sayHello() {
        System.out.println("Hello World");
        return "Hi there";
    }
}
```

```java
public static void main(String[] args) {
    ApplicationContext ctx = SpringApplication.run(SfgDiApplication.class, args);

    MyController myController = (MyController) ctx.getBean("myController");
    String greetings = myController.sayHello();
    System.out.println(greetings);
}
```

## Types of Dependency injection in Spring Framework

Here is a three types of injections but only the first one if preferred and recommended by the community:

- Constructor injected

```java
@Controller
public class ConstructorInjectedController {

    private final GreetingService greetingService;

    public ConstructorInjectedController(@Qualifier("constructorInjectedGreetingServiceImpl") GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String getGreeting() {
        return greetingService.sayGreeting();
    }
}
```

- Setter injected

```java
@Controller
public class SetterInjectedController {

    private GreetingService greetingService;

    @Qualifier("setterInjectedGreetingServiceImpl")
    @Autowired
    public void setGreetingService(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String getGreeting() {
        return greetingService.sayGreeting();
    }
}
```

- Property injected

```java
@Controller
public class PropertyInjectedController {

    @Qualifier("propertyInjectedGreetingServiceImpl")
    @Autowired
    public GreetingService greetingService;

    public String getGreeting() {
        return greetingService.sayGreeting();
    }
}
```

## Annotations

### `@Autowired`

In Spring, we are using `@Autowired` annotation to tell the Spring to inject other beans into that place. 
Implementations of the `GreetingService` interface has a `@Service` annotations so spring will manage to inject them into a bean with `@Autowired` annotation.

Please note that there is no `@Autowired` annotation in case of the Constructor based injection. 
Since to create and use that bean we need to use a constructor an `@Autowired` annotation can be omitted in that case.

### `@Qualifier`

In our case `GreetingService` is an interface that have 3 implementations for each of the types of injection. 
If there will be only one implementation of that interface `@Qualifier` annotation will not be needed as Spring inject correct implementation as a single representation of the injected interface. 
Here `@Qualifier` will tell the Spring to use exactly that implementation on the `GreetingService` interface.

### `@Primary`

Primary annotation allow you to set a bean as a primary. It means if there are more than one implementation but `@Qualifier` is not specified use bean with `@Primary` annotation as a default one.

Example:

```java
@Controller
public class MyController {

    private final GreetingService greetingService;

    public MyController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String getGreeting() {
        return greetingService.sayGreeting();
    }
}
```

```java
@Primary
@Service
public class PrimaryGreetingService implements GreetingService {
    @Override
    public String sayGreeting() {
        return "Hello World - From Primary Service";
    }
}
```

### `@Profile`

In the case when we want to load bean one for production and different for development we can do this based on `@Profile` annotation.
`@Profile` annotations will tell when to load beans and inject them into context based on selected profile.

Example:

```java
@Controller
public class I18nController {

    private final GreetingService greetingService;

    public I18nController(@Qualifier("i18nService") GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String getGreeting() {
        return greetingService.sayGreeting();
    }
}
```

```java
@Profile("EN")
@Service("i18nService")
public class I18nEnglishGreetingService implements GreetingService {

    @Override
    public String sayGreeting() {
        return "Hello World - EN";
    }
}
```

```java
@Profile("ES")
@Service("i18nService")
public class I18nSpainGreetingService implements GreetingService {

    @Override
    public String sayGreeting() {
        return "Hola Mundo - ES";
    }
}
```

application.properties:

```properties
spring.profiles.active=ES
```

Here we can see that two services created with the same bean name *i18nService* and controller will don't know which one to load in that case. Here come a `@Profile` annotation. 
After we add it into our services and specify in the application properties which one to use in that case spring will load correct bean into the application context and inject it into the controller for us.


If we would like to use one of the beans as a default one we can specify it in the `@Profile` annotation. We can do this by adding a *"default"* to the list of `@Profile` values like this. 

```java
@Profile({"ES", "default"})
```